// Bài tập 1: in mộ bảng số từ 1 -100
function inBangCoDieuKien() {
  var content = "";
  var contentHTML = "";

  for (var i = 1; i <= 100; i += 10) {
    for (j = 0; j < 10; j++) {
      var num = i + j;
      content = num + " | ";
      contentHTML += content;
      if (num >= 100) {
        break;
      }
    }
    contentHTML += "<br>";
  }
  document.getElementById("result-ex-1").innerHTML = contentHTML;
}

// Bài tập 2: Số nguyên tố
var arrEx2 = [];
function layGiaTriVaoMang() {
  if (document.getElementById("txt-number-ex2").value.trim() == "") {
    return;
  }
  var numberEx2 = document.getElementById("txt-number-ex2").value * 1;
  arrEx2.push(numberEx2);
  document.getElementById("txt-number-ex2").value = "";

  document.getElementById("arr-ex2").innerHTML = `${arrEx2}`;
}

function inRaSoNguyenTo() {
  function kiemTraSoNguyenTo(n) {
    var checkSNT = true;
    if (n < 2) {
      checkSNT = false;
    } else {
      for (var i = 2; i < n - 1; i++) {
        if (n % i == 0) {
          checkSNT = false;
          break;
        }
      }
    }
    return checkSNT;
  }

  var arrSNT = [];
  arrEx2.forEach(function (item) {
    if (kiemTraSoNguyenTo(item) == true) {
      arrSNT.push(item);
    }
  });

  if (arrSNT.length > 0) {
    document.getElementById(
      "result-ex-2"
    ).innerHTML = ` Số nguyên tố trong mảng: ${arrSNT}`;
  } else {
    document.getElementById(
      "result-ex-2"
    ).innerHTML = `Mảng không có số nguyên tố nào`;
  }
}

// Bài tập 3: Viết function nhận vào tham số n, tính S=(2+3+4...+n)+2n
function tinhTong() {
  var numberEx3 = document.getElementById("txt-number-ex3").value * 1;

  var sum = 0;
  for (i = 2; i <= numberEx3; i++) {
    sum += i;
  }
  sum = sum + 2 * numberEx3;

  document.getElementById("result-ex-3").innerHTML = `Kết quả là: ${sum}`;
}

// Bài tập 4: Tính số lượng ước số của n
function timUocSo() {
  var numberEx4 = document.getElementById("txt-number-ex4").value * 1;

  var uocSo = "";

  for (var i = 1; i <= numberEx4; i++) {
    if (numberEx4 % i == 0) {
      uocSo = uocSo + i + " ";
    }
  }
  document.getElementById(
    "result-ex-4"
  ).innerHTML = `Ước số của ${numberEx4} là: ${uocSo}`;
}

// Bài tập 5: Tìm số đảo ngược
function timSoDaoNguoc() {
  var stringNumber = document.getElementById("txt-number-ex5").value;
  var newStringNumber = "";

  for (let i = stringNumber.length - 1; i >= 0; i--) {
    newStringNumber += stringNumber[i];
  }

  document.getElementById(
    "result-ex-5"
  ).innerHTML = ` Số đảo ngược là: ${newStringNumber}`;
}

// Bài tập 6: Tìm X
function timSoX() {
  var sum = 0;
  var x;
  for (i = 1; sum < 100; i++) {
    sum += i;
    if (sum > 100) {
      break;
    }
    x = i;
  }

  document.getElementById("result-ex-6").innerHTML = `X = ${x}`;
}

// Bài tập 7: In bảng cửu chương

function inBangCuuChuong() {
  var numberEx7 = document.getElementById("txt-number-ex7").value * 1;
  var result;
  var cuuChuong = "";
  for (var i = 0; i <= 10; i++) {
    result = numberEx7 * i;
    cuuChuong += `${numberEx7} x ${i} = ${result}` + "</br>";
  }

  document.getElementById("result-ex-7").innerHTML = `${cuuChuong}`;
}

// Bài tập 8: Viết hàm chia bài:
function chiaBai() {
  var players = [[], [], [], []];

  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];

  cards.forEach(function (item, index) {
    players[index % 4].push(item);
  });

  document.getElementById(
    "result-ex-8"
  ).innerHTML = `${players[0]} </br> ${players[1]} </br> ${players[2]} </br>${players[3]}`;
}

// Bài tập 9: tính số gà và số chó

function tinhSoGaSoCho() {
  var tongSoGaVaCho = document.getElementById("tongSoGa-Cho").value * 1;
  var tongSoChan = document.getElementById("tongSoChan").value * 1;
  if (
    (4 * tongSoGaVaCho - tongSoChan) % 2 == 0 &&
    4 * tongSoGaVaCho - tongSoChan >= 2 &&
    (tongSoChan - 2 * tongSoGaVaCho) % 2 == 0 &&
    tongSoChan - 2 * tongSoGaVaCho >= 2
  ) {
    var soGa = (4 * tongSoGaVaCho - tongSoChan) / 2;
    var soCho = (tongSoChan - 2 * tongSoGaVaCho) / 2;
    document.getElementById(
      "result-ex-9"
    ).innerHTML = ` <div>Số lượng chó là: ${soCho}</div> <div>Số lượng gà là: ${soGa}</div>`;
  } else {
    document.getElementById("result-ex-9").innerHTML = ` Số nhập không hợp lệ`;
  }
}

// Bài tập 10: Nhập vào số giờ và số phút => góc lệch giữa kim giờ và kim phút

function gocLech() {
  var kimGio = document.getElementById("kimGio").value * 1;
  var kimPhut = document.getElementById("kimPhut").value * 1;

  if (kimGio > 12) {
    alert("Số giờ phải nhỏ hơn hoặc bằng 12");
    return;
  }

  if (kimPhut > 60) {
    alert("Số giờ phải nhỏ hơn hoặc bằng 60");
    return;
  }

  var gocLech = Math.abs(30 * kimGio - (11 / 2) * kimPhut);

  document.getElementById(
    "result-ex-10"
  ).innerHTML = ` Góc lệch là: ${gocLech}`;
}
